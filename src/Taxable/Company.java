package Taxable;

public class Company implements Taxable  {
	private String name ;
	private double in ;
	private double out;
	
	public Company(String name,double in ,double out){
		this.name = name ;
		this.in  = in;
		this.out = out;
	}
	@Override
	public double getTax() {
	 double tax = 0;
	     tax = (this.in - this.out )*0.3;
		return tax;
	}
	

}
