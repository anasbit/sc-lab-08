package Taxable;

import java.util.ArrayList;

public class Calculate {
	
	public static double cal(ArrayList<Taxable> list){
		double x = 0 ;
		for(Taxable t : list){
			x = x + t.getTax() ;
		}
		return x;
		
	}
}
