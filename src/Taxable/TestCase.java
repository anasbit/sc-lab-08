package Taxable;

import java.util.ArrayList;

public class TestCase {

	public static void main(String[] args) {
		System.out.println("------------Different Types------------" );
		ArrayList<Taxable> pList = new ArrayList<Taxable>();
		pList.add(new Person("jeng", 11111));
		pList.add(new Person("gist", 23456));
		pList.add(new Person("kan", 555555));
        
		double sum = Calculate.cal(pList);
		System.out.println("Taxes of persons : " + sum);

		ArrayList<Taxable> cList = new ArrayList<Taxable>();
		cList.add(new Company("vShape cream", 3000000, 750000));
		cList.add(new Company("Umbrella", 1500000, 654300));
		cList.add(new Company("McDunold", 6300000, 300000));

		double tax1 = Calculate.cal(cList);
		System.out.println("Taxes of companies : " + tax1);

		ArrayList<Taxable> proList = new ArrayList<Taxable>();
		proList.add(new Product("Mama", 200));
		proList.add(new Product("Waiwai", 500));
		proList.add(new Product("Yamyam", 800));

		double tax2 = Calculate.cal(proList);
		System.out.println("Taxes of products : " + tax2);
		
		System.out.println("------------All Types------------" );
		ArrayList<Taxable> all = new ArrayList<Taxable>();
		all.add(new Person("jeng", 11111));
		all.add(new Person("gist", 23456));
		all.add(new Person("kan", 555555));
		all.add(new Company("vShape cream", 3000000, 750000));
		all.add(new Company("Umbrella", 1500000, 654300));
		all.add(new Company("McDunold", 6300000, 300000));
		all.add(new Product("Mama", 200));
		all.add(new Product("Waiwai", 500));
		all.add(new Product("Yamyam", 800));
		double totalTax = Calculate.cal(all);
		System.out.println("Taxes of all types : " + totalTax);

	}

}
