package Taxable;

public class Person implements Taxable {
	private double salary;
	private String name;
	

	public Person( ) {
		salary = 0 ;
	}
	public Person(String name, double perYear) {
		this.name = name;
		salary = perYear;
	}

	@Override
	public double getTax() {
		double tax = 0;
		if (salary < 300000) {
			tax = (tax + (salary * 0.05));

		}
		else
		if (salary >= 300000) {
			tax = (tax + (300000 * 0.05));

		}
		if (salary > 300000 && salary <= 500000) {
			tax = (tax + ((salary - 300000) * 0.1));

		}
		if (salary > 500000 && salary <= 750000) {

			tax = (tax + ((salary - 500000) * 0.15));

		}
		return tax;
	}

}
