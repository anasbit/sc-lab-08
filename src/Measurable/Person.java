package Measurable;

public class Person implements Measurable {
	private String name ;
	private double avg;
	private double high ;
	
	public Person(String name ,double high){
		this.name = name ; 
		this.high = high ;
	}
	public Person(double avgHigh){
		high = avgHigh ;
	}

	@Override
	public double getMeasure() {
		return high;
	}

}
