package Measurable;

public class Calculate {
	
	public static double avg(Measurable[] x) {
		double sum = 0;
		for (Measurable m : x) {
			sum = sum + m.getMeasure();
		}
		if (x.length > 0) {
			return sum / x.length;
		} 
		else {
			return 0;
		}
	}
	public static Measurable min(Measurable m1, Measurable m2) {
		if(m1.getMeasure() < m2.getMeasure()){
			
			return m1;
		}
		return m2;
		
	}
}
