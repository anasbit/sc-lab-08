package Measurable;

public class BankAccount implements Measurable {
	private String name ;
	private double money;
	

	public BankAccount() {
		money = 0;
	}

	public BankAccount(double balance) {
		money = balance;
	}
	
	public double getBalance() {
		return money;
	}

	public void deposit(double amount) {
		money = money + amount;
	}

	public void withdraw(double amount) {
		money = money - amount;
	}

	

	@Override
	public double getMeasure() {
		return money;
	}

}
