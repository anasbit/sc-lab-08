package Measurable;

public class Main {
		public static void main(String[] args){
			Measurable [] person = new Measurable[5];
			person[0] = new Person("good",105);
			person[1] = new Person("guy",195);
			person[2]= new Person("may",138);
			person[3] = new Person("be",179);
			person[4] = new Person("gay",155);
		    
			double high = Calculate.avg(person) ;
			System.out.println("Average of persons : " +high);
			Measurable  m1 = new Person(high);
			
			Measurable [] country = new Measurable[5];
			country[0] = new Country("Australia",123456);
			country[1] = new Country("Canada",55555);
			country[2] = new Country("Dubai",696969);
			country[3] = new Country("Gana",9087234);
			country[4] = new Country("Thailand",1001001);
			
			double avgArea = Calculate.avg(country) ;
			System.out.println("Average of area : " +avgArea);
			Measurable  m2 = new Country(avgArea);
			
			Measurable [] id = new Measurable[5];
			id[0]  = new BankAccount(10);
			id[1]  = new BankAccount(680);
			id[2]  = new BankAccount(2300);
			id[3]  = new BankAccount(28860);
			id[4]  = new BankAccount(62700);
			
			double avgId = Calculate.avg(id) ;
			System.out.println("Average of money : " +avgId); 
			Measurable m3 = new BankAccount(avgId);
			Measurable min = Calculate.min(m1,m2);
			Double a = min.getMeasure();
			System.out.println("Average of persons : " +a+ " less than average' s area : "+m2.getMeasure());	
			Measurable min2 = Calculate.min(m3, m1);
			Double b = min2.getMeasure();
			System.out.println("Average of persons : " +b+ " less than  average' s accounts: "+m3.getMeasure());
			Measurable min3 = Calculate.min(m3, m2);
			Double c = min3.getMeasure();
			System.out.println("Average of accounts : " +c+ " less than  average' s area: "+m2.getMeasure());
			
		}
}
