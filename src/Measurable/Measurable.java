package Measurable;

public interface Measurable {
   double getMeasure();
}
